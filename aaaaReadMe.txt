This repository includes the app directory for the multiplication table coding test.

Implementing this system would first require a base copy of Magento2.
The app directory in this base copy should be replaced with the app directory from the repository.

This can then be run with e.g. http://localhost/app/multiplcation/index/index, with localhost replaced
with the actual location and path where the initial Magento2 copy was located.