<?php
declare(strict_types=1);
namespace ECtest\Multiplication\Block;
 
class Multiplication extends \Magento\Framework\View\Element\Template
{
    public function getMultiplicationHtml(): string
    {
    	#
    	#    Build multiplication table using html table
    	#
    	$html = "<table border=1 cellpadding=2 cellspacing=2>\n";
		foreach(range(0,12) as $down) {
    		$html .=  " <tr>\n";
    		foreach(range(0,12) as $across) {
        		if ($down == 0 and $across == 0) {
            		$html .=  "  <td bgcolor='#cccccc'>X</td>\n";
        		} elseif ($down == 0) {
            		$html .=  "  <td bgcolor='#cccccc'>$across</td>\n";
        		} elseif ($across == 0) {
            		$html .=  "  <td bgcolor='#cccccc'>$down</td>\n";
        		} else {
            		$html .=  "  <td>" . $down * $across . "</td>\n";
        		}
    		}
    		$html .=  " </tr>\n";
		}
		$html .= "</table>";

        return $html;
    }

        public function getMultiplicationCss(): string
    {
    	#
    	#    Build multiplication table using css grid
    	#
        $html = "<div class='container'>\n";
        foreach(range(0,12) as $down) {
            $html .=  " <tr>\n";
            foreach(range(0,12) as $across) {
                if ($down == 0 and $across == 0) {
                    $html .= "    <div class='header'>X</div>\n";
                } elseif ($down == 0) {
                    $html .= "    <div class='header'>$across</div>\n";
                } elseif ($across == 0) {
                    $html .= "    <div class='header'>$down</div>\n";
                } else {
                    $html .= "    <div class='item'>" . $down * $across . "</div>\n";
                }
            }
        }
        $html .= "</div>\n";

        return $html;
    }
}