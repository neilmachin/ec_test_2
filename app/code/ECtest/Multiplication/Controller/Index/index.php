<style>
    .container {
        display: grid;
        width: 500px;
        height: 300px;
        margin: 100px auto 0;
        grid-template-columns: repeat(13, auto);
        grid-template-rows: repeat(13, auto);
        grid-gap: 5px;
    }

    .item {
        border: 1px solid black;
        text-align: center;
    }

    .header {
        background: rgb(240,240,240);
        text-align: center;
    }
</style>

<?php
 
namespace ECtest\Multiplication\Controller\Index;
 
use Magento\Framework\App\Action\Context;
 
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
 
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
 
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }
}